// SPDX-License-Identifier: AGPL-3.0-only
// Copyright (C) 2024  kalyd

use std::{
	fmt,
	io::{self,Write},
	process::exit
};
use rand::{thread_rng,seq::SliceRandom};

const FEATURES : usize = 4;
const VALUES   : usize = 3;
const COLOR_RESET   : &str = "\x1b[m";
const COLOR_BOLD    : &str = "\x1b[1m";
const COLOR_SUCCESS : &str = "\x1b[32m";
const COLOR_FAILURE : &str = "\x1b[31m";

type SET = (Card, Card, Card);

#[derive(Clone,Copy)]
struct Card {
	color:   usize,
	number:  usize,
	shape:   usize,
	shading: usize,
}
impl fmt::Display for Card {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "{}{}{}", COLOR_RESET, format_card(self), COLOR_RESET)
	}
}

fn format_card(card: &Card) -> String {
	let color = match card.color {
		0 => "\x1b[35m",
		1 => "\x1b[33m",
		2 => "\x1b[36m",
		_ => "\x1b[37m",
	};
	let shape = match card.shape {
		1 => 'I',
		2 => 'X',
		_ => 'O',
	};
	let sym = format!("{}{}{}{}", COLOR_BOLD, color, shape, COLOR_RESET);
	let fill = match card.shading {
		1 => '-',
		2 => '#',
		_ => ' ',
	};
	match card.number {
		1 => format!("{}{}{}{}{}", fill, sym, fill, sym, fill),
		2 => format!("{}{}{}{}{}", sym, fill, sym, fill, sym),
		_ => format!("{}{}{}{}{}", fill, fill, sym, fill, fill),
	}
}

fn msg(color: &str, s: &str) {
	println!("{}{}{}{}", COLOR_BOLD, color, s, COLOR_RESET);
}

fn draw_set((c0,c1,c2): SET, border_color: &str, text: &str) {
	let delim = format!("{}│", border_color);
	println!("{}┌───────┬───────┬───────┐{}", border_color, COLOR_RESET);
	println!("{} {} {} {} {} {} {} {}{}{}", delim, c0, delim, c1, delim, c2, delim, COLOR_BOLD, text, COLOR_RESET);
	println!("{}└───────┴───────┴───────┘{}", border_color, COLOR_RESET);
}

fn compatible(v0: usize, v1: usize, v2: usize) -> bool {
	v0 == v1 && v1 == v2 || v0 != v1 && v1 != v2 && v2 != v0
}

fn is_set((c0,c1,c2): SET) -> bool {
	compatible(c0.color, c1.color, c2.color) &&
	  compatible(c0.number, c1.number, c2.number) &&
	  compatible(c0.shape, c1.shape, c2.shape) &&
	  compatible(c0.shading, c1.shading, c2.shading)
}

fn find_set(f: &Vec<Card>) -> Option<SET> {
	let size = f.len();
	for c0 in 0..size {
		for c1 in c0+1..size {
			for c2 in c1+1..size {
				let set = (f[c0], f[c1], f[c2]);
				if is_set(set) {
					return Some(set);
				}
			}
		}
	}
	None
}

// LinkedList would be more appropriate but Vec simply works
fn gen_cards() -> Vec<Card> {
	let mut cards = Vec::<Card>::with_capacity(VALUES^FEATURES);
	for color in 0..VALUES {
		for number in 0..VALUES {
			for shape in 0..VALUES {
				for shading in 0..VALUES {
					let card = Card {
						color:   color,
						number:  number,
						shape:   shape,
						shading: shading,
					};
					cards.push(card);
				}
			}
		}
	}
	let mut rng = thread_rng();
	cards.shuffle(&mut rng);
	cards
}

fn main() {
	let fieldsize = FEATURES * VALUES;
	let mut cards = gen_cards();
	let mut field = Vec::<Card>::new();
	for _ in 0..fieldsize {
		field.push(cards.pop().unwrap());
	}

	loop {
		// ensure existence of SET; if not, extend field or end game
		let help_set : SET;
		loop {
			match find_set(&field) {
				Some(set) => {
					help_set = set;
					break;
				},
				None => {
					if cards.len() >= VALUES {
						for _ in 0..VALUES {
							field.push(cards.pop().unwrap());
						}
					} else {
						msg(COLOR_SUCCESS, "congratulations! you found all SETs!");
						exit(0);
					}
				},
			}
		}

		// print field
		println!("find a {}SET{}! {} cards left", COLOR_BOLD, COLOR_RESET, cards.len());
		println!("┌───────┬───────┬───────┐");
		for (i, f) in field.iter().enumerate() {
			print!("│ {} ", f);
			if (i+1) % VALUES == 0 {
				println!("│");
				if (i+1) != field.len() {
					println!("├───────┼───────┼───────┤");
				}
			}
		}
		println!("└───────┴───────┴───────┘");

		// read input
		let mut set_ix : Vec<usize>;
		loop {
			print!("input card IDs [x,y,z]: ");
			io::stdout().flush().unwrap();
			let mut buf = String::new();
			io::stdin().read_line(&mut buf).unwrap();
			buf.pop(); // remove trailing newline
			match buf.as_str() {
				"quit" | "exit" => {
					exit(0);
				},
				"help" => {
					draw_set(help_set, COLOR_RESET, "this SET exists");
					continue;
				},
				_ => {
					let nums : Vec<&str> = buf.split(",").collect();
					if nums.len() != VALUES {
						msg(COLOR_FAILURE, &format!("select exactly {} cards", VALUES));
						continue;
					}
					set_ix = Vec::with_capacity(VALUES);
					for n in nums.iter() {
						match n.parse() {
							Ok(i)  => set_ix.push(i),
							Err(e) => msg(COLOR_FAILURE, &format!("{} is not a valid card ID: {}", n, e)),
						}
					}
					if set_ix.len() != VALUES {
						continue;
					}
					if set_ix[0] == set_ix[1] || set_ix[1] == set_ix[2] || set_ix[2] == set_ix[0] {
						msg(COLOR_FAILURE, "select different cards");
						continue;
					}
					if *set_ix.iter().max().unwrap() >= field.len() {
						msg(COLOR_FAILURE, "card IDs must not exceed field size");
						continue;
					}
					let set : SET = (field[set_ix[0]], field[set_ix[1]], field[set_ix[2]]);
					if is_set(set) {
						draw_set(set, COLOR_SUCCESS, "you found a SET!");
						break;
					} else {
						draw_set(set, COLOR_FAILURE, "sadly, this is not a SET. try again!");
					}
				}
			}
		}

		// process field
		set_ix.sort();
		set_ix.reverse();
		if field.len() > fieldsize || cards.len() < VALUES {
			for i in set_ix.iter() {
				if *i == field.len()-1 {
					field.pop().unwrap();
				} else {
					let c = field.pop().unwrap();
					field[*i] = c;
				}
			}
		} else {
			for i in set_ix.iter() {
				field[*i] = cards.pop().unwrap();
			}
		}
	}
}
