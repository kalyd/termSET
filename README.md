# termSET
Terminal-based SET game written in Rust.

SET is a fast-paced card game where the challenge is to match cards
based on patterns.

The 4 pattern features are implemented as follows:
 - Color: cyan, magenta, or yellow
 - Number: 1, 2, or 3 symbols
 - Shape: `X`, `I`, or `O`
 - Shading: implemented as shading *between* the symbols using `#`, `-`, or spaces

## Usage
Run `cargo run --release` to compile the project and its dependencies
locally, and automatically run the built afterwards.

termSET guarantees to always have a SET present, and fill in cards
dynamically.

You will be prompted to input 3 comma-separated card indices which form a
SET. The cards are indexed from top left to bottom right, starting at 0.
The indices may be given in any order.

If you have trouble finding the current SET, you may type `help`.

To quit the game before it is finished, type `quit` or `exit`.

## License
This project is licensed under AGPL-3.0-only.

To make switching to a later version possible, all contributions and
pull requests must be licensed under AGPL-3.0-or-later.
